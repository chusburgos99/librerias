﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibreriaVisual;

namespace ConsolaVisual
{
    class Program
    {
        public static void Main(string[] args)
        {

            Console.WriteLine("Elige una opcion");
            Console.WriteLine("1.Suma(s) -  2.Resta(r) - 3.Multiplicacion(m) -  4.Division(d)");
            String opcion = System.Console.ReadLine();

            if (opcion == "s")
            {
                LibreriaVisual.Class1.suma(7,8);
            }

            if (opcion == "r")
            {
                LibreriaVisual.Class1.resta(5, 2);
            }

            if (opcion == "m")
            {
                LibreriaVisual.Class1.multiplicacion(8, 9);
            }

            if (opcion == "d")
            {
                LibreriaVisual.Class1.division(24, 6);
            }
            
           
        }
    }
}
