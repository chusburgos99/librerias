package prueba;

import java.util.Scanner;

import main.Operaciones;

public class Main {
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner in = new Scanner (System.in);
		
		
		System.out.println("Elige una opcion");
		System.out.println("1.Suma - 2.Resta - 3.Multiplicacion - 4.Division");
		int opcion=in.nextInt();
		
		if(opcion == 1){
			System.out.println("Introduce el numero 1:");
			int num1=in.nextInt();
			System.out.println("Introduce el numero 2:");
			int num2=in.nextInt();
			Operaciones.suma(num1, num2);
		}
		
		if(opcion == 2){
			System.out.println("Introduce el numero 1:");
			int num1=in.nextInt();
			System.out.println("Introduce el numero 2:");
			int num2=in.nextInt();
			Operaciones.resta(num1, num2);
		}
		
		if(opcion == 3){
			System.out.println("Introduce el numero 1:");
			int num1=in.nextInt();
			System.out.println("Introduce el numero 2:");
			int num2=in.nextInt();
			Operaciones.multiplicacion(num1, num2);
		}
		
		if(opcion == 4){
			System.out.println("Introduce el numero 1:");
			int num1=in.nextInt();
			System.out.println("Introduce el numero 2:");
			int num2=in.nextInt();
			Operaciones.division(num1, num2);
		}
		
	}

}
