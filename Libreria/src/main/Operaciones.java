package main;


public class Operaciones {

	 public static int suma(int num1, int num2){
		 int resultadoSuma = num1 + num2;
		 System.out.println(num1 + "+" + num2 + "= " + resultadoSuma);
		 return resultadoSuma;
     }
	 
     public static int resta(int num1, int num2){
    	 int resultadoResta = num1 - num2;
		 System.out.println(num1 + "-" + num2 + "= " + resultadoResta);
		 return resultadoResta;
     }
     
     public static int multiplicacion(int num1, int num2){
    	 int resultadoMulti = num1 * num2;
		 System.out.println(num1 + "*" + num2 + "= " + resultadoMulti);
		 return resultadoMulti;
     }
     
     public static int division(int num1, int num2){
    	 int resultadoDivision = num1 / num2;
		 System.out.println(num1 + "/" + num2 + "= " + resultadoDivision);
		 return resultadoDivision;
     }
}
